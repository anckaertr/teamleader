
// Dependencies
var express = require('express');
var router = express.Router();
var jsonParser = require('../jsonParser.js');


// JSON data (instead of DB like mongodb);
var customers = require('../data/customers.json');
var products = require('../data/products.json');
var reductions = require('../data/reductions.json');


// dummy data and run test()
/*
var defaultquotations = {
  "id": "3",
  "customer-id": "3",
  "items": [
    {
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "product-id": "A102",
      "quantity": "1",
      "unit-price": "49.50",
      "total": "49.50"
    }
  ],
  "total": "69.00"
};

handleData(defaultquotations);

*/


// set up post handling
router.post("/", function (req, res) {

    handlePost(req.body, res);

});


// initiate checks
function handlePost(data, res){

    data = handleData(data);

    if(data !== false){
        res.status(200);
        res.send(data);
    }
    else {
        res.status(500);
        res.send('You need a customer-id and at least 1 product to use this service.');
    }

}

// check if there is a customer or items in the json
// and continue with the reduction calculation
function handleData (data){

    var customer = getCustomerData(data);

    var originalData = JSON.parse(JSON.stringify(data));
    // check if a customer was found (uses customer data)
    if(customer !== false){
        data.customer = customer;

        // check if there are products in the post-data
        if(data.items.length > 0){
            var reduction = calculateReduction(data);

            var returnValue = buildReturnString(originalData, reduction);

            return returnValue;
        }

        else {
            // no products
            console.log('there are no products');
            return false;
        }
    }
    else{
        // non existing customer.
        console.log('This customer does not exist.');
        return false;
    }

}


//
function buildReturnString(data, reduction){
    data.reductions = reduction.reductions;
    data.totalWithReductions = reduction.totalWithReductions;
    return data;
}


// split reductions between 'global' and 'product' and handle accordingly
function calculateReduction (data){

    var validReductions = [];

    var len = reductions.length;
    for(var i=0; i<len; i++){

        var reduction  = reductions[i];

        if(reduction.type == 'global'){
            if(doGlobalReductionEval(data, reduction.rules)){
                validReductions.push(reduction);
            }
        }
        else if(reduction.type == 'product'){
            var productReduction = doProductReductionEval(data, reduction);
            if(productReduction !== false){

                reduction.appliedReductions = productReduction.appliedReductions;
                reduction.reductionAmount = productReduction.reductionAmount;

                validReductions.push(reduction);
            }
        }

    }


    // check if there are reductions applied
    // if true, calculate our total price with reductions and return all valid reductions + reduced total
    if(validReductions.length > 0){

        var totalWithReductions = calculatePriceWithReductions(validReductions, data);

        return {
            'reductions': validReductions,
            'totalWithReductions': totalWithReductions
        };
    }
    else {
        return false;
    }

}


//
function calculatePriceWithReductions(reductions, data){


    var total = parseFloat(data.total);

    var globalReductions = getItemsFromArrayByRules(reductions, "item.type == 'global'");
    var productReductions = getItemsFromArrayByRules(reductions, "item.type == 'product'");

    var len = productReductions.length;
    for(var i = 0; i<len; i++){
        total -= productReductions[i].reductionAmount;
    }

    len = globalReductions.length;
    for(var i = 0; i<len; i++){
        total = eval(reductions[i].reduction);
    }

    return total;

}

// handle global reduction
function doGlobalReductionEval(data, rules){

    if(eval(rules)){
        return true;
    }
    else {
        return false;
    }

}


// handle product reduction
function doProductReductionEval(data, reduction){

    var reductionApplicable = false;
    var reductionAmount = 0;
    var appliedReductions = [];

    var filteredProducts = getItemsFromArrayByRules(data.items, reduction.selector);

    if(filteredProducts.length <=0 ){
        return false;
    }
    if(reduction.sort){
        filteredProducts = sortArrayByRules(filteredProducts, reduction.sort);
    }

    // if perProduct is true, check each product seperately
    if(reduction.perProduct === true){
        var len = filteredProducts.length;
        for(var i = 0; i < len; i++){
            var item = filteredProducts[i];
            if(eval(reduction.rules)){
                reductionApplicable = true;
                item.reductionAmount = parseFloat(eval(reduction.reduction));
                reductionAmount += item.reductionAmount;
                appliedReductions.push(item);
            }
        }
    }
    else {
        var items = filteredProducts;
        if(eval(reduction.rules)){
            reductionApplicable = true;
            reductionAmount = parseFloat(eval(reduction.reduction));
            appliedReductions = items;
        }
    }

    if(reductionApplicable === true){
        return {
            'reductionAmount' : reductionAmount,
            'appliedReductions': appliedReductions
        };
    }
    else{
        return false;
    }

}


// sort the array according to the params set in the reduction
function sortArrayByRules(array, param){

    function compare(a,b) {

      if (eval('a' + param) < eval('b'+param))
        return -1;
      if (eval('a' + param) > eval('b'+param))
        return 1;
      return 0;

    }

    return array.sort(compare);

}


// get the corresponding items according to the selector set in the reduction
function getItemsFromArrayByRules(array, selector){

    var filterFunction = function(item){
        getProductData(item);
        return eval(selector);
    };
    return array.filter(filterFunction);

}


// get product by id
function getProductData(item){

    item.jsonData = jsonParser.getObjects(products,'id', item['product-id'])[0];
    return item;

}


// check if customer exists and return the customer data
function getCustomerData(data){

    var customer = jsonParser.getObjects(customers,'id', data['customer-id']);
    if(customer.length>0){
        return customer[0];
    }
    else {
        return false;
    }

}


// Return router
module.exports = router;
