//Dependencies
var express = require('express');
//var mongoose = require('mongoose');
var bodyParser = require('body-parser');


/* We could also use mongodb
// MongoDB
mongoose.connect('mongodb://localhost/teamleader');
*/

// Express
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Routes
app.use('/api', require('./routes/api'));
app.use('/api/reduction', require('./routes/reduction'));

app.listen(3000);
console.log("API is running on port 3000");
