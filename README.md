# README #

Code test for teamleader (see [https://bitbucket.org/teamleadercrm/coding-test/overview](https://bitbucket.org/teamleadercrm/coding-test/overview))

### Setup ###

#### Requirements ####

* [Nodejs](https://nodejs.org/en/)
* npm (included with nodejs)


#### Setup ####

* open terminal or bash tool
* go to target directory: 
`cd myDirectoryLocation/myDirectory`
* clone this repository: 
`git clone https://anckaertr@bitbucket.org/anckaertr/teamleader.git`

* go into teamleader folder
`cd teamleader`
* download and install dependencies with
`npm install`
* start the node server using `node server.js`

The api should be running on http://localhost:3000/api;


### testing the service ###

service url: http://localhost:3000/api/reduction/

You can use [postman](https://www.getpostman.com/) to test the service.

Use the 'POST' with the quotation as data (see examples in the quotations folder)
Be sure to set your data as 'JSON (application/json)'

![Screen Shot 2016-08-10 at 20.26.08.png](https://bitbucket.org/repo/K8oebn/images/2840670521-Screen%20Shot%202016-08-10%20at%2020.26.08.png)